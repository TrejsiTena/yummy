import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';

class RecipiesScreen extends StatefulWidget {
  static const String id = 'recipies_screen';
  @override
  _RecipiesScreenState createState() => _RecipiesScreenState();
}

class _RecipiesScreenState extends State<RecipiesScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Yummy"),
        backgroundColor: Hexcolor('#465570'),
      ),
      body: SafeArea(
          child: Container(
        color: Hexcolor('#5A6D8F'),
      )),
      bottomNavigationBar: BottomNavigationBar(
          backgroundColor: Hexcolor('#465570'),
          type: BottomNavigationBarType.fixed,
          items: <BottomNavigationBarItem>[
            BottomNavigationBarItem(
                icon: Icon(Icons.home),
                title: new Text('Home'),
                backgroundColor: Hexcolor('#465570'),),
            BottomNavigationBarItem(
                icon: Icon(Icons.favorite),
                title: new Text('Favorite'),
                backgroundColor: Hexcolor('#465570')),
            BottomNavigationBarItem(
                icon: Icon(Icons.folder_special),
                title: new Text('More'),
                backgroundColor: Hexcolor('#465570')),
            BottomNavigationBarItem(
                icon: Icon(Icons.settings),
                title: new Text('Settings'),
                backgroundColor: Hexcolor('#465570')),
          ]),
    );
  }
}
