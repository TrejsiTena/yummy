import 'package:flutter/material.dart';
import 'rounded_button.dart';
import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:recipies_app/login_screen.dart';
import 'package:recipies_app/register_screen.dart';

class HomeScreen extends StatefulWidget {
  static const String id='home_screen';
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(padding: EdgeInsets.symmetric(horizontal: 26.0),
          color: Hexcolor('#465570'),
          child: SafeArea(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Hero(
                      tag: 'logo',
                      child: Container(
                        child: Image.asset('images/logo.png'),
                        height: 60.0,
                        width: 60.0,
                      ),
                    ),
                    TyperAnimatedTextKit(
                      text: ['Y u m m y'],
                      textStyle: TextStyle(
                        color: Hexcolor('#ffbfbf'),
                        fontSize: 45.0,
                        fontWeight: FontWeight.w900,
                        fontFamily: 'HomemadeApple',
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 48.0,
                ),
                RoundedButton(
                  colour: Hexcolor('#D28080'),//#d28080
                  title: 'Log In',
                  onPressed: () {
                    Navigator.pushNamed(context, LoginScreen.id);
                  },
                ),
                RoundedButton(
                  colour: Hexcolor('#994D4F'),//#8a6283
                  title: 'Register',
                  onPressed: () {
                    Navigator.pushNamed(context, RegistrationScreen.id);
                  },
                )
              ],
            ),
          ),
        ),
      ),

    );
  }
}
